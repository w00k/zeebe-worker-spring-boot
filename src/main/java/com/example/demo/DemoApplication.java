package com.example.demo;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import io.zeebe.client.api.response.ActivatedJob;
import io.zeebe.client.api.worker.JobClient;
import io.zeebe.spring.client.EnableZeebeClient;
import io.zeebe.spring.client.annotation.ZeebeWorker;
import jdk.internal.jline.internal.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EnableZeebeClient
@Slf4j
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @ZeebeWorker(type = "helloworld-service")
    public void handleHelloWorldServiceCoordination(final JobClient client, final ActivatedJob job) {
        log.info("### type helloworld-service");
        OkHttpClient clientHttp = new OkHttpClient();
        String messageToString = "";
        String responseMessage = "";
        String url = ""; 
        Map<String, String> message = new HashMap<>();
        try {
            Map<String, Object> variables = job.getVariablesAsMap();
            log.info("url : " + job.getCustomHeaders().get("url"));
            log.info("variables : " + variables.get("name").toString());
            
            url = job.getCustomHeaders().get("url").toString() + "/" + variables.get("name").toString();          
            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();
            
            Response response = clientHttp.newCall(request).execute();
            responseMessage = response.body().string();
            log.info("### Response : " + responseMessage);

            JsonObject jsonMessage = new Gson().fromJson(responseMessage, JsonObject.class);
            messageToString = jsonMessage.getAsJsonPrimitive("message").getAsString();
            log.info("### message : " + messageToString);
            message.put("message", messageToString);
            
            logJob(job);
            client.newCompleteCommand(job.getKey()).variables(message).send().join();

        } catch (IOException e) {
            log.error("exception JobFail");
            logJob(job);
            client.newFailCommand(job.getKey());
        }
    }

    private static void logJob(final ActivatedJob job) {
        log.info(
                "complete job\n>>> [type: {}, key: {}, element: {}, workflow instance: {}]\n{deadline; {}]\n[headers: {}]\n[variables: {}]",
                job.getType(),
                job.getKey(),
                job.getElementId(),
                job.getWorkflowInstanceKey(),
                Instant.ofEpochMilli(job.getDeadline()),
                job.getCustomHeaders(),
                job.getVariables());
    }

}
